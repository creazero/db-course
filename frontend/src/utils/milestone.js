export function getProgress(milestone) {
    if (milestone.num_issues === 0) {
        return 0;
    }
    return Math.ceil((milestone.num_closed / milestone.num_issues) * 100);
}
