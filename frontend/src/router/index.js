import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import Signup from "../views/Signup";
import Home from "../views/Home";
import NewRepository from "../views/NewRepository";
import { authenticationGuard, fetchUserData } from "./guards";
import Repository from "../views/Repository";
import IssuesView from "../views/repository/IssuesView";
import NewIssue from "../views/repository/NewIssue";
import IssuePage from "../views/repository/IssuePage";
import LabelsView from "../views/repository/LabelsView";
import HomeView from "../views/HomeView";
import MilestonesView from "../views/repository/MilestonesView";
import MilestonePage from "../views/repository/MIlestonePage";
import NewMilestone from "../views/repository/NewMilestone";
import RepoSettings from "../views/repository/RepoSettings";
import UserPage from "../views/user/UserPage";
import EditUserPage from "../views/user/EditUserPage";
import NotFound from "../views/errors/NotFound";
import ChangePasswordPage from "../views/user/ChangePasswordPage";
import GlobalIssueSearchPage from "../views/GlobalIssueSearchPage";

Vue.use(VueRouter);

const routes = [
    {
        path: "/login",
        name: "login",
        component: Login
    },
    {
        path: "/signup",
        name: "signup",
        component: Signup
    },
    {
        path: "/404",
        component: NotFound,
        name: "not_found"
    },
    {
        path: "/",
        name: "home",
        component: Home,
        children: [
            {
                path: "",
                name: "home_view",
                component: HomeView
            },
            {
                path: "/issue_search",
                component: GlobalIssueSearchPage,
                name: "issue_search"
            },
            {
                path: "new",
                name: "new_repository",
                component: NewRepository
            },
            {
                path: ":username",
                name: "user_page",
                component: UserPage
            },
            {
                path: ":username/edit",
                name: "edit_user",
                component: EditUserPage
            },
            {
                path: ":username/change_password",
                name: "change_password",
                component: ChangePasswordPage
            },
            {
                path: ":username/:repo_name",
                component: Repository,
                children: [
                    {
                        path: "",
                        name: "repo_issues",
                        component: IssuesView
                    },
                    {
                        path: "issues/new",
                        name: "new_issue",
                        component: NewIssue
                    },
                    {
                        path: "issues/:issueId",
                        name: "issue",
                        component: IssuePage
                    },
                    {
                        path: "issues/:issueId/edit",
                        name: "edit_issue",
                        component: NewIssue
                    },
                    {
                        path: "labels",
                        name: "labels",
                        component: LabelsView
                    },
                    {
                        path: "milestones",
                        name: "milestones",
                        component: MilestonesView
                    },
                    {
                        path: "milestones/new",
                        name: "new_milestone",
                        component: NewMilestone
                    },
                    {
                        path: "milestones/:milestoneId/edit",
                        name: "edit_milestone",
                        component: NewMilestone
                    },
                    {
                        path: "milestones/:milestoneId",
                        name: "milestone",
                        component: MilestonePage
                    },
                    {
                        path: "settings",
                        name: "repo_settings",
                        component: RepoSettings
                    }
                ]
            }
        ]
    },
    {
        path: "*",
        component: NotFound
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

router.beforeEach(authenticationGuard);
router.beforeEach(fetchUserData);

export default router;
