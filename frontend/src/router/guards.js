import store from "../store";
import { FETCH_CURRENT_USER } from "../store/auth";

export function authenticationGuard(to, from, next) {
    const iaAuthenticated = !!localStorage.getItem("token");
    if (to.name !== "login" && to.name !== "signup" && !iaAuthenticated) {
        next("/login");
    } else {
        next();
    }
}

export function fetchUserData(to, from, next) {
    const iaAuthenticated = !!localStorage.getItem("token");
    const isUserDataPresent = !!store.state.currentUser;
    if (iaAuthenticated && !isUserDataPresent) {
        store.dispatch(FETCH_CURRENT_USER);
    }
    next();
}
