import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import "./plugins/element.js";
import "./assets/styles.scss";

Vue.config.productionTip = false;

axios.interceptors.request.use(request => {
    request.headers["Authorization"] = `Bearer ${localStorage.getItem(
        "token"
    )}`;
    return request;
}, null);

axios.interceptors.response.use(null, error => {
    if (error.response.status === 422 || error.response.status === 401) {
        store.commit("logout");
        router.push({ name: "login" });
    }
    return Promise.reject(error);
});

axios.interceptors.response.use(null, error => {
    if (error.response.status === 404 || error.response.status === 403) {
        router.push({ name: "not_found" });
    }
    return Promise.reject(error);
});

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
