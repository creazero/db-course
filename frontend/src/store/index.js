import Vue from "vue";
import Vuex from "vuex";
import auth from "./auth";
import repository from "./repository";
import issue from "./issues";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        auth,
        repositoryState: repository,
        issuesState: issue
    }
});
