import axios from "axios";
import { DECREASE_ISSUES_COUNT, INCREASE_ISSUES_COUNT } from "./repository";

export const FETCH_ISSUES_FOR_REPO_ACTION = "fetchIssuesForRepo";
export const FETCH_ISSUE_ACTION = "fetchIssue";
export const CREATE_ISSUE_ACTION = "createIssue";
export const FETCH_ISSUE_COMMENTS_ACTION = "fetchIssueComments";
export const CREATE_COMMENT_ACTION = "createComment";
export const PATCH_ISSUE_ACTION = "patchIssue";
export const FETCH_LABELS_ACTION = "fetchLabels";
export const FETCH_MILESTONES_ACTION = "fetchMilestones";
export const DELETE_ISSUE_ACTION = "deleteIssue";
export const DELETE_MILESTONE_ACTION = "deleteMilestone";
export const DELETE_LABEL_ACTION = "deleteLabel";

export const CLEAR_ISSUE_MUTATION = "clearIssue";
const SET_ISSUE_MUTATION = "setIssue";
const CLEAR_ISSUES_MUTATION = "clearIssues";
const SET_COMMENTS = "setComments";
const CLEAR_COMMENTS = "clearComments";
const ADD_COMMENT = "addComment";
const SET_LABELS = "setLabels";
const CLEAR_LABELS = "clearLabels";
const REMOVE_MILESTONE = "removeMilestone";
export const ADD_LABEL = "addLabel";
export const UPDATE_LABEL = "updateLabel";
export const CLEAR_MILESTONES = "clearMilestones";
export const SET_MILESTONES = "setMilestones";
export const REMOVE_LABEL = "removeLabel";

export default {
    state: {
        issues: [],
        singleIssue: null,
        comments: [],
        labels: [],
        milestones: []
    },
    mutations: {
        setIssues(state, issues) {
            state.issues = issues;
        },
        [CLEAR_ISSUES_MUTATION](state) {
            state.issues = [];
        },
        setIssue(state, issue) {
            state.singleIssue = issue;
        },
        [CLEAR_ISSUE_MUTATION](state) {
            state.singleIssue = null;
        },
        [SET_COMMENTS](state, comments) {
            state.comments = comments;
        },
        [CLEAR_COMMENTS](state) {
            state.comments = [];
        },
        [ADD_COMMENT](state, comment) {
            state.comments = [...state.comments, comment];
        },
        [SET_LABELS](state, labels) {
            state.labels = labels;
        },
        [CLEAR_LABELS](state) {
            state.labels = [];
        },
        [ADD_LABEL](state, label) {
            state.labels = [...state.labels, label];
        },
        [UPDATE_LABEL](state, label) {
            const labelIndex = state.labels.findIndex(
                fLabel => fLabel.id === label.id
            );
            state.labels[labelIndex] = { ...label };
            state.labels = [...state.labels];
        },
        [CLEAR_MILESTONES](state) {
            state.milestones = [];
        },
        [SET_MILESTONES](state, milestones) {
            state.milestones = milestones;
        },
        [REMOVE_MILESTONE](state, milestoneId) {
            state.milestones = state.milestones.filter(
                milestone => milestone.id !== milestoneId
            );
        },
        [REMOVE_LABEL](state, labelId) {
            state.labels = state.labels.filter(label => label.id !== labelId);
        }
    },
    actions: {
        async [FETCH_ISSUES_FOR_REPO_ACTION]({ commit }, repoId) {
            commit(CLEAR_ISSUES_MUTATION);
            const response = await axios.get(
                `/api/issues?repositoryId=${repoId}`
            );
            commit("setIssues", response.data.issues);
        },
        async [FETCH_ISSUE_ACTION]({ commit }, issueId) {
            commit(CLEAR_ISSUE_MUTATION);
            const response = await axios.get(`/api/issues/${issueId}`);
            commit("setIssue", response.data);
            return response.data;
        },
        async [CREATE_ISSUE_ACTION]({ commit }, issueData) {
            const response = await axios.post("/api/issues", issueData);
            commit(INCREASE_ISSUES_COUNT, null, { root: true });
            return response.data;
        },
        async [FETCH_ISSUE_COMMENTS_ACTION]({ commit }, issueId) {
            commit(CLEAR_COMMENTS);
            const response = await axios.get(
                `/api/comments?issueId=${issueId}`
            );
            commit(SET_COMMENTS, response.data.comments);
        },
        async [CREATE_COMMENT_ACTION]({ commit }, commentData) {
            const response = await axios.post("/api/comments", commentData);
            commit(ADD_COMMENT, response.data);
        },
        async [PATCH_ISSUE_ACTION]({ commit }, { issueId, patchData }) {
            const response = await axios.patch(
                `/api/issues/${issueId}`,
                patchData
            );
            commit(SET_ISSUE_MUTATION, response.data);
            return response.data;
        },
        async [FETCH_LABELS_ACTION]({ commit }, repoId) {
            commit(CLEAR_LABELS);
            const response = await axios.get(`/api/labels?repoId=${repoId}`);
            commit(SET_LABELS, response.data.labels);
            return response.data.labels;
        },
        async [FETCH_MILESTONES_ACTION]({ commit }, repoId) {
            commit(CLEAR_MILESTONES);
            const response = await axios.get(
                `/api/milestones?repoId=${repoId}`
            );
            commit(SET_MILESTONES, response.data.milestones);
            return response.data.milestones;
        },
        async [DELETE_ISSUE_ACTION]({ commit }, issueId) {
            const response = await axios.delete(`/api/issues/${issueId}`);
            commit(CLEAR_ISSUE_MUTATION);
            commit(DECREASE_ISSUES_COUNT);
            return response.data;
        },
        async [DELETE_MILESTONE_ACTION]({ commit }, milestoneId) {
            const response = await axios.delete(
                `/api/milestones/${milestoneId}`
            );
            commit(REMOVE_MILESTONE, milestoneId);
            return response.data;
        },
        async [DELETE_LABEL_ACTION]({ commit }, labelId) {
            const response = await axios.delete(`/api/labels/${labelId}`);
            commit(REMOVE_LABEL, labelId);
            return response.data;
        }
    }
};
