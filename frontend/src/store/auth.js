import axios from "axios";

export const LOGIN_ACTION = "login";
export const FETCH_CURRENT_USER = "fetchUserData";
export const LOGOUT_MUTATION = "logout";
export const FETCH_USER_ACTION = "fetchUser";
export const PATCH_USER_ACTION = "patchUser";

export const SET_USER_MUTATION = "setUser";
export const CLEAR_USER_MUTATION = "clearUser";

export default {
    state: {
        currentUser: null,
        user: null
    },
    getters: {
        username: state => state.currentUser?.username
    },
    mutations: {
        setUserData(state, userData) {
            state.currentUser = userData;
        },
        [LOGOUT_MUTATION](state) {
            state.currentUser = null;
            const token = localStorage.getItem("token");
            if (token) {
                localStorage.removeItem("token");
            }
        },
        [SET_USER_MUTATION](state, userData) {
            state.user = userData;
        },
        [CLEAR_USER_MUTATION](state) {
            state.user = null;
        }
    },
    actions: {
        async [LOGIN_ACTION]({ commit }, { username, password }) {
            return new Promise((resolve, reject) => {
                axios
                    .post("/api/login", { username, password })
                    .then(response => {
                        commit("setUserData", response.data);
                        localStorage.setItem("token", response.data.token);
                        resolve(response.data);
                    })
                    .catch(reject);
            });
        },
        async [FETCH_CURRENT_USER]({ commit }) {
            const response = await axios.get("/api/current_user");
            commit("setUserData", response.data);
        },
        async [FETCH_USER_ACTION]({ commit }, username) {
            commit(CLEAR_USER_MUTATION);
            const response = await axios.get(`/api/users/${username}`);
            commit(SET_USER_MUTATION, response.data);
            return response.data;
        },
        async [PATCH_USER_ACTION]({ commit }, { userId, patchData }) {
            const response = await axios.patch(
                `/api/users/${userId}`,
                patchData
            );
            commit(SET_USER_MUTATION, response.data);
            return response.data;
        }
    }
};
