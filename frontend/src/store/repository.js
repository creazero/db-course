import axios from "axios";

export const FETCH_REPOSITORY_ACTION = "fetchRepository";
export const TOGGLE_WATCH_ACTION = "toggleWatch";
export const FETCH_COLLABORATORS_ACTION = "fetchCollaborators";
export const PATCH_REPOSITORY_ACTION = "patchRepository";
export const DELETE_REPOSITORY_ACTION = "deleteRepository";
export const FETCH_REPOS_FOR_USER = "fetchReposForUser";

export const SET_REPOSITORY_MUTATION = "setRepository";
export const CLEAR_REPOSITORY_MUTATION = "clearRepository";
export const SET_COLLABORATORS_MUTATION = "setCollaborators";
export const CLEAR_COLLABORATORS_MUTATION = "cleanCollaborators";
export const SET_WATCH_COUNT_MUTATION = "setWatchCount";
export const INCREASE_ISSUES_COUNT = "increaseIssuesCount";
export const DECREASE_ISSUES_COUNT = "decreaseIssuesCount";
const UPDATE_REPOSITORY = "updateRepository";
const SET_USER_REPOS = "setUserRepos";
const CLEAR_USER_REPOS = "clearUserRepos";

export default {
    state: {
        repository: null,
        collaborators: [],
        userRepos: []
    },
    mutations: {
        [SET_REPOSITORY_MUTATION](state, repository) {
            state.repository = repository;
        },
        [CLEAR_REPOSITORY_MUTATION](state) {
            state.repository = null;
        },
        [SET_WATCH_COUNT_MUTATION](state, count) {
            state.repository.num_watches = count;
        },
        [SET_COLLABORATORS_MUTATION](state, collaborators) {
            state.collaborators = collaborators;
        },
        [CLEAR_COLLABORATORS_MUTATION](state) {
            state.collaborators = [];
        },
        [INCREASE_ISSUES_COUNT](state) {
            state.repository.num_issues++;
        },
        [DECREASE_ISSUES_COUNT](state) {
            state.repository.num_issues--;
        },
        [UPDATE_REPOSITORY](state, updatedRepository) {
            state.repository = {
                ...state.repository,
                ...updatedRepository
            };
            state.collaborators = updatedRepository.contributors;
        },
        [SET_USER_REPOS](state, repos) {
            state.userRepos = repos;
        },
        [CLEAR_USER_REPOS](state) {
            state.userRepos = [];
        }
    },
    actions: {
        async [FETCH_REPOSITORY_ACTION]({ commit }, { username, repoName }) {
            commit(CLEAR_REPOSITORY_MUTATION);
            const url = `${username}/${repoName}`;
            const response = await axios.get(`/api/repositories/${url}`);
            commit(SET_REPOSITORY_MUTATION, response.data);
        },
        async [TOGGLE_WATCH_ACTION]({ commit }, repoId) {
            const response = await axios.post(
                `/api/repositories/${repoId}/watch`
            );
            commit(SET_WATCH_COUNT_MUTATION, response.data.count);
            return response.data.count;
        },
        async [FETCH_COLLABORATORS_ACTION]({ commit }, repoId) {
            const response = await axios.get(
                `/api/repositories/${repoId}/contributors`
            );
            commit(SET_COLLABORATORS_MUTATION, response.data.users);
            return response.data;
        },
        async [PATCH_REPOSITORY_ACTION](
            { commit },
            { repositoryId, patchData }
        ) {
            const response = await axios.patch(
                `/api/repositories/${repositoryId}`,
                patchData
            );
            commit(UPDATE_REPOSITORY, response.data);
            return response.data;
        },
        async [DELETE_REPOSITORY_ACTION]({ commit }, repositoryId) {
            const response = await axios.delete(
                `/api/repositories/${repositoryId}`
            );
            commit(CLEAR_REPOSITORY_MUTATION);
            return response.data;
        },
        async [FETCH_REPOS_FOR_USER]({ commit }, userId) {
            commit(CLEAR_USER_REPOS);
            const response = await axios.get(
                `/api/repositories?user_id=${userId}`
            );
            commit(SET_USER_REPOS, response.data.repos);
            return response.data.repos;
        }
    }
};
