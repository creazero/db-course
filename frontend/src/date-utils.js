import { formatDistanceToNow } from "date-fns";
import { ru } from "date-fns/locale";

export function getDistanceToNow(date) {
    const convertedDate = new Date(date);
    return formatDistanceToNow(convertedDate, { addSuffix: true, locale: ru });
}
