from flask import Flask, Response, make_response, jsonify, g
from flask_jwt_extended import JWTManager

from git_viewer import db, commands
from git_viewer.api import users, repositories, issues, comments, labels, milestones
from git_viewer.config import Config
from git_viewer.exceptions import APIException, InvalidDBConnection
from git_viewer.jwt import jwt_identity, identity_loader

CONNECTION_POOL = None


def create_app() -> Flask:
    global CONNECTION_POOL

    app = Flask('git_viewer')
    app.config.from_object(Config())

    register_routes(app)
    register_commands(app)
    register_extensions(app)

    app.register_error_handler(APIException, api_exception_handler)
    app.before_request(before_request)
    app.after_request(after_request)

    with app.app_context():
        CONNECTION_POOL = db.get_db_pool()

    return app


def register_routes(app: Flask) -> None:
    app.register_blueprint(users.router, url_prefix='/api')
    app.register_blueprint(repositories.router, url_prefix='/api')
    app.register_blueprint(issues.router, url_prefix='/api')
    app.register_blueprint(comments.router, url_prefix='/api')
    app.register_blueprint(labels.router, url_prefix='/api')
    app.register_blueprint(milestones.router, url_prefix='/api')


def register_commands(app: Flask) -> None:
    app.cli.add_command(commands.init_db, 'init_db')
    app.cli.add_command(commands.drop_db, 'drop_db')
    app.cli.add_command(commands.generate_data, 'generate')


def register_extensions(app: Flask) -> None:
    jwt = JWTManager(app)
    jwt.user_loader_callback_loader(jwt_identity)
    jwt.user_identity_loader(identity_loader)


def api_exception_handler(error: APIException) -> Response:
    return make_response(jsonify(error.to_dict()), error.status_code)


def before_request():
    connection = CONNECTION_POOL.getconn()
    if not connection:
        raise InvalidDBConnection
    g.db = connection


def after_request(resp):
    if 'db' in g:
        CONNECTION_POOL.putconn(g.db)
    return resp
