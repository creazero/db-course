class Config:
    DEBUG = True
    SECRET_KEY = 'unUsnAe2BGb3AkjKDXmo9lE7Gc9qbERP'
    DB_HOST = 'localhost'
    DB_USER = 'git_viewer_admin'
    DB_PASSWORD = '123456'
    DB_PORT = 5433
    DB_NAME = 'git_viewer'
    REPOS_PATH = '/Users/creazero/repos'


class ProductionConfig(Config):
    DB_PORT = 5432
