import psycopg2
from flask import current_app
from psycopg2 import pool


def get_db_pool() -> pool.SimpleConnectionPool:
    common_kwargs = get_common_kwargs()
    return pool.SimpleConnectionPool(
        minconn=1,
        maxconn=20,
        **common_kwargs
    )


def get_db_connection():
    common_kwargs = get_common_kwargs()
    return psycopg2.connect(**common_kwargs)


def get_common_kwargs() -> dict:
    return {
        'host': current_app.config['DB_HOST'],
        'user': current_app.config['DB_USER'],
        'password': current_app.config['DB_PASSWORD'],
        'port': current_app.config['DB_PORT'],
        'dbname': current_app.config['DB_NAME']
    }
