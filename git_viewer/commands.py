import os.path
import random

import click
import psycopg2
from faker import Faker
from flask import g
from flask.cli import with_appcontext

from git_viewer.db import get_db_connection
from git_viewer.models.comment import CommentType, Comment
from git_viewer.models.issue import Issue
from git_viewer.models.label import Label
from git_viewer.models.milestone import Milestone
from git_viewer.models.repository import Repository
from git_viewer.models.user import User

CREATE_FILE = 'sql/create.sql'
DROP_FILE = 'sql/drop.sql'


@click.command('init_db')
@with_appcontext
def init_db():
    _run_file(CREATE_FILE)
    print('Database has been initialized')


@click.command('drop_db')
@with_appcontext
def drop_db():
    _run_file(DROP_FILE)
    print('All tables have been dropped')


@click.command('generate')
@with_appcontext
def generate_data():
    users_count = 100
    repos_count = 300
    milestone_count = 100
    issue_count = 500
    comment_count = 1_000
    label_count = repos_count * 2
    per_issue_label_count = 2

    conn = get_db_connection()
    g.db = conn

    with conn:
        generate_users(conn, users_count)
        generate_repos(conn, repos_count, users_count)
        generate_milestones(conn, milestone_count, repos_count)
        generate_labels(conn, label_count, repos_count)
        generate_access(conn, repos_count, users_count)
        generate_issues(conn, issue_count, repos_count, users_count)
        generate_comments(conn, comment_count, issue_count, users_count)
        conn.commit()
    conn.close()


def generate_users(conn, count: int) -> None:
    fake = Faker('ru_RU')
    with conn:
        for _ in range(count):
            profile = fake.simple_profile()
            user = User()
            user.username = profile['username']
            user.full_name = profile['name']
            user.email = profile['mail']
            password = fake.lexify(text='????????')
            user.password = User.hash_password(password)
            print(f'{user.username} - {password}')
            try:
                user.save()
            except:
                continue
        print('Пользователи сгенерированы')


def generate_repos(conn, count: int, users_count: int) -> None:
    fake = Faker()
    with conn:
        words = fake.words(nb=count, unique=True)
        for i in range(count):
            repo = Repository()
            repo.name = words[i]
            repo.description = fake.text(max_nb_chars=150)
            repo.owner_id = random.randint(1, users_count)
            repo.is_private = random.randint(0, 100) < 30
            repo.save(commit=False)
        print('Репозитории сгенерированы')


def generate_milestones(conn, count: int, repos_count: int) -> None:
    fake = Faker()
    with conn:
        for _ in range(count):
            milestone = Milestone()
            milestone.repo_id = random.randint(1, repos_count)
            milestone.title = ' '.join(fake.words(nb=2))
            milestone.description = fake.text(max_nb_chars=1000)
            if random.randint(0, 100) > 15:
                milestone.due_date = fake.date_between(start_date='-1y', end_date='+1y')
            milestone.save(commit=False)
        print('Этапы сгенерированы')


def generate_labels(conn, count: int, repos_count: int) -> None:
    fake = Faker()
    with conn:
        for _ in range(count):
            label = Label()
            label.title = f'{fake.word()}_module'[:20]
            label.description = fake.text(max_nb_chars=1000)
            label.color = fake.color()
            label.repo_id = random.randint(1, repos_count)
            label.save(commit=False)
        print('Теги сгенерированы')


def generate_access(conn, repos_count: int, users_count: int) -> None:
    with conn:
        for i in range(1, repos_count):
            repo_access_count = random.randint(0, 10)
            if repo_access_count == 0:
                continue
            repository = Repository.get_by_id(i)
            added_users = []
            while len(added_users) != repo_access_count:
                user_id = random.randint(1, users_count)
                if user_id in added_users or repository.owner_id == user_id:
                    continue
                added_users.append(user_id)
                repository.add_contributor(user_id, commit=False)
        print('Доступы сгенерированы')


def generate_issues(conn, issues_count: int, repos_count: int, users_count: int) -> None:
    ru_fake = Faker('ru_RU')
    with conn:
        for _ in range(issues_count):
            repo_id = random.randint(1, repos_count)
            repository = Repository.get_by_id(repo_id)
            contributors = repository.get_contributors()
            contributor_ids = [contributor.id for contributor in contributors]
            milestone_ids = [
                milestone.id
                for milestone in Milestone.get_from_repo(repo_id)
            ]
            if repository.is_private:
                author_id = contributor_ids[random.randint(0, len(contributor_ids) - 1)]
            else:
                author_id = random.randint(1, users_count)
            possible_assignees = [author_id] + contributor_ids
            assignee_id = None
            if random.randint(0, 100) > 55:
                assignee_id = possible_assignees[random.randint(0, len(possible_assignees) - 1)]
            milestone_id = None
            if len(milestone_ids) > 0 and random.randint(0, 100) > 90:
                if len(milestone_ids) == 1:
                    milestone_id = milestone_ids[0]
                else:
                    milestone_id = milestone_ids[random.randint(0, len(milestone_ids) - 1)]
            available_labels = [
                label.id
                for label in Label.get_from_repo(repository.id)
            ]
            issue = Issue.create_issue(**{
                'author_id': author_id,
                'repo_id': repo_id,
                'title': ru_fake.text(max_nb_chars=100),
                'content': ru_fake.text(max_nb_chars=10_000),
                'assignee_id': assignee_id,
                'milestone_id': milestone_id,
                'commit': False
            })
            if len(available_labels) > 0 and random.randint(0, 100) > 50:
                if len(available_labels) == 1:
                    labels_count = 1
                else:
                    labels_count = random.randint(1, len(available_labels) - 1)
                added_labels = []
                while len(added_labels) < labels_count:
                    label_pos = random.randint(0, len(available_labels) - 1)
                    if available_labels[label_pos] in added_labels:
                        continue
                    added_labels.append(available_labels[label_pos])
                    issue.add_label(available_labels[label_pos], commit=False)
        print('Задачи сгенерированы')


def generate_comments(conn, comments_count: int, issues_count: int, users_count: int) -> None:
    ru_fake = Faker('ru_RU')
    with conn:
        for _ in range(comments_count):
            issue_id = random.randint(1, issues_count)
            issue = Issue.get_by_id(issue_id)
            repository = Repository.get_by_id(issue.repo_id)
            if repository.is_private:
                contributor_ids = [contributor.id for contributor in repository.get_contributors()]
                author_id = contributor_ids[random.randint(0, len(contributor_ids) - 1)]
            else:
                author_id = random.randint(1, users_count)
            comment_type = CommentType.PLAIN
            comment = Comment()
            comment.author_id = author_id
            comment.type = comment_type
            comment.issue_id = issue.id
            comment.repo_id = issue.repo_id
            comment.content = ru_fake.text(max_nb_chars=10_000)
            comment.save(commit=False)
        print('Коменты сгенерированы')


def _run_file(filename: str) -> None:
    base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    full_path = os.path.join(base_path, filename)
    with get_db_connection() as conn:
        with conn.cursor() as cursor:
            cursor.execute(open(full_path, 'r').read())
