class APIException(Exception):
    status_code = 500
    message = 'Неизвестная ошибка'

    def to_dict(self):
        return {
            'message': self.message
        }


class InvalidDBConnection(APIException):
    message = 'Cannot connect to the database'


class NotUniqueCredentials(APIException):
    status_code = 400
    message = 'Данные пользователя должны быть уникальными'


class InvalidCredentials(APIException):
    status_code = 401
    message = 'Неправильные имя пользователя или пароль'


class OccupiedRepositoryName(APIException):
    status_code = 400
    message = 'Такая комбинация пользователя и названия репозитория уже занята'


class UnknownRepository(APIException):
    status_code = 404
    message = 'Такого репозитория не существует'


class UnknownIssue(APIException):
    status_code = 404
    message = 'Такой задачи не существует'


class UnknownLabel(APIException):
    status_code = 404
    message = 'Такого тега не существует'


class UnknownMilestone(APIException):
    status_code = 404
    message = 'Такого этапа не существует'


class UnknownUser(APIException):
    status_code = 404
    message = 'Такого пользователя не существует'
