import datetime
from typing import List, Optional

from flask import g
from psycopg2.extras import DictCursor


class Milestone:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id', None)
        self.repo_id = kwargs.get('repo_id', None)
        self.title = kwargs.get('title', None)
        self.description = kwargs.get('description', None)
        self.num_issues = kwargs.get('num_issues', None)
        self.due_date = kwargs.get('due_date', None)
        self.created_at = kwargs.get('created_at', None)
        self.updated_at = kwargs.get('updated_at', None)

        self.num_closed = 0

    def delete(self) -> None:
        delete_query = 'DELETE FROM milestone WHERE id = %s;'
        with g.db.cursor() as cursor:
            cursor.execute(delete_query, (self.id,))

    def save(self, commit=True) -> None:
        insert_query = '''
        INSERT INTO milestone (repo_id, title, description, due_date)
            VALUES (%(repo_id)s, %(title)s, %(description)s, %(due_date)s)
            RETURNING *;
        '''
        update_query = '''
        UPDATE milestone
            SET title = %(title)s,
                description = %(description)s,
                due_date = %(due_date)s,
                updated_at = %(updated_at)s
            WHERE id = %(id)s;
        '''
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            if self.id is None:
                cursor.execute(
                    insert_query,
                    {
                        'repo_id': self.repo_id,
                        'title': self.title,
                        'description': self.description,
                        'due_date': self.due_date
                    }
                )
                db_row = cursor.fetchone()
                self.id = db_row.get('id', None)
                self.num_issues = db_row.get('num_issues', None)
                self.due_date = db_row.get('due_date', None)
                self.created_at = db_row.get('created_at', None)
                self.updated_at = db_row.get('updated_at', None)
            else:
                self.updated_at = datetime.datetime.now()
                cursor.execute(
                    update_query,
                    {
                        'title': self.title,
                        'description': self.description,
                        'updated_at': self.updated_at,
                        'due_date': self.due_date,
                        'id': self.id
                    }
                )
            if commit:
                g.db.commit()

    @staticmethod
    def get_by_id(milestone_id: int) -> Optional['Milestone']:
        select_query = 'SELECT * FROM milestone WHERE id = %s;'
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(select_query, (milestone_id,))
            db_row = cursor.fetchone()
            return Milestone(**db_row) if db_row is not None else None

    @staticmethod
    def get_from_repo(repo_id: int) -> List['Milestone']:
        select_query = 'SELECT * FROM milestone WHERE repo_id = %s;'
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(select_query, (repo_id,))
            db_rows = cursor.fetchall()
        return [Milestone(**db_row) for db_row in db_rows]
