import enum
from typing import Optional, List

from flask import g
from psycopg2.extras import DictCursor

from git_viewer.models.user import User


class CommentType(enum.Enum):
    PLAIN = 0
    CLOSED = 1
    REOPEN = 2


class Comment:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id', None)
        self.type = kwargs.get('type', CommentType.PLAIN)
        if type(self.type) == int:
            self.type = CommentType(self.type)
        self.author_id = kwargs.get('author_id', None)
        self.issue_id = kwargs.get('issue_id', None)
        self.repo_id = kwargs.get('repo_id', None)
        self.content = kwargs.get('content', None)
        self.created_at = kwargs.get('created_at', None)
        self.updated_at = kwargs.get('updated_at', None)

        self.author = None

    def save(self, commit=True) -> None:
        insert_query = '''
        INSERT INTO comment (type, author_id, issue_id, repo_id, content)
        VALUES (%(type)s, %(author_id)s, %(issue_id)s, %(repo_id)s, %(content)s)
        RETURNING *;
        '''
        update_query = '''
        UPDATE comment
            SET content = %(content)s,
                updated_at = current_timestamp
            WHERE %(id)s;
        '''
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            if self.id is None:
                cursor.execute(
                    insert_query,
                    {
                        'type': self.type.value,
                        'author_id': self.author_id,
                        'issue_id': self.issue_id,
                        'repo_id': self.repo_id,
                        'content': self.content
                    }
                )
                db_row = cursor.fetchone()
                self.id = db_row['id']
                self.created_at = db_row['created_at']
                self.updated_at = db_row['updated_at']
            else:
                cursor.execute(
                    update_query,
                    {
                        'content': self.content,
                        'id': self.id
                    }
                )
            self.author = User.get_by_id(self.author_id)
            if commit:
                g.db.commit()

    @staticmethod
    def create_plain_comment(author_id: int, issue_id: int,
                             repo_id: int, content: str) -> 'Comment':
        comment = Comment(
            author_id=author_id,
            issue_id=issue_id,
            repo_id=repo_id,
            content=content,
            type=CommentType.PLAIN
        )
        comment.save()
        return comment

    @staticmethod
    def create_state_comment(author_id: int, issue_id: int,
                             repo_id: int, is_closed: bool) -> 'Comment':
        comment = Comment(
            author_id=author_id,
            issue_id=issue_id,
            repo_id=repo_id,
            type=CommentType.CLOSED if is_closed else CommentType.REOPEN
        )
        comment.save()
        return comment

    @staticmethod
    def get_by_issue_id(issue_id: int) -> List['Comment']:
        select_query = 'SELECT * FROM comment WHERE issue_id = %s ORDER BY updated_at;'
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(select_query, (issue_id,))
            db_rows = cursor.fetchall()
        comments = [Comment(**db_row) for db_row in db_rows]
        for comment in comments:
            comment.author = User.get_by_id(comment.author_id)
        return comments
