from typing import Optional, List

from flask import g

from git_viewer.models.label import Label
from git_viewer.models.milestone import Milestone
from git_viewer.models.user import User


class Issue:
    def __init__(self, data: tuple):
        iterator = iter(data)
        self.id = next(iterator)
        self.author_id = next(iterator)
        self.repo_id = next(iterator)
        self.title = next(iterator)
        self.content = next(iterator)
        self.assignee_id = next(iterator)
        self.is_closed = next(iterator)
        self.milestone_id = next(iterator)
        self.num_comments = next(iterator)
        self.created_at = next(iterator)
        self.updated_at = next(iterator)

        self.author = None
        self.assignee = None
        self.labels = []
        self.milestone = None
        self.repository = None

    def add_label(self, label_id: int, commit=True) -> None:
        insert_query = '''
        INSERT INTO issue_tag (label_id, issue_id) VALUES (%s, %s);
        '''
        with g.db.cursor() as cursor:
            cursor.execute(insert_query, (label_id, self.id))
            if commit:
                g.db.commit()
        label = Label.get_by_id(label_id)
        self.labels.append(label)

    def replace_labels(self, ids: List[int]) -> None:
        current_ids = [label.id for label in self.labels]
        if current_ids == ids:
            return
        delete_query = 'DELETE FROM issue_tag WHERE issue_id = %s;'
        with g.db:
            with g.db.cursor() as cursor:
                cursor.execute(delete_query, (self.id,))
                g.db.commit()
                self.labels = []
            for label_id in ids:
                self.add_label(label_id)

    def merge(self, patch_data: dict) -> 'Issue':
        if 'repo_id' in patch_data:
            self.repo_id = patch_data.get('repo_id')
        if 'title' in patch_data:
            self.title = patch_data.get('title')
        if 'content' in patch_data:
            self.content = patch_data.get('content')
        if 'assignee_id' in patch_data:
            self.assignee_id = patch_data.get('assignee_id')
        if 'is_closed' in patch_data:
            self.is_closed = patch_data.get('is_closed')
        if 'milestone_id' in patch_data:
            self.milestone_id = patch_data.get('milestone_id')
        return self

    def delete(self) -> None:
        delete_query = 'DELETE FROM issue WHERE id = %s;'
        with g.db.cursor() as cursor:
            cursor.execute(delete_query, (self.id,))
            g.db.commit()

    def save(self, commit=True) -> None:
        update_query = '''
        UPDATE issue
            SET repo_id = %s,
                title = %s,
                content = %s,
                assignee_id = %s,
                is_closed = %s,
                milestone_id = %s
            WHERE id = %s;
        '''
        with g.db.cursor() as cursor:
            cursor.execute(
                update_query,
                (
                    self.repo_id,
                    self.title,
                    self.content,
                    self.assignee_id,
                    self.is_closed,
                    self.milestone_id,
                    self.id
                )
            )
            if commit:
                g.db.commit()

    @staticmethod
    def create_issue(commit=True, **issue_data: dict) -> 'Issue':
        insert_query = '''
        INSERT INTO issue (author_id, repo_id, title, content, assignee_id, milestone_id)
        VALUES (%s, %s, %s, %s, %s, %s)
        RETURNING id;
        '''
        with g.db.cursor() as cursor:
            cursor.execute(
                insert_query,
                (
                    issue_data.get('author_id'),
                    issue_data.get('repo_id'),
                    issue_data.get('title'),
                    issue_data.get('content'),
                    issue_data.get('assignee_id', None),
                    issue_data.get('milestone_id', None)
                )
            )
            new_id = cursor.fetchone()[0]
            if commit:
                g.db.commit()
        return Issue.get_by_id(new_id)

    @staticmethod
    def search(repo_id: Optional[int], assignee_id: Optional[int],
               owner_id: Optional[int], is_closed=Optional[bool]) -> List['Issue']:
        query = 'SELECT * FROM issue'
        has_previous_case = False
        if (repo_id is not None or assignee_id is not None
                or owner_id is not None or is_closed is not None):
            query += ' WHERE'
        if repo_id is not None:
            query += ' repo_id = %(repo_id)s'
            has_previous_case = True
        if assignee_id is not None:
            if has_previous_case:
                query += ' AND'
            query += ' assignee_id = %(assignee_id)s'
            has_previous_case = True
        if owner_id is not None:
            if has_previous_case:
                query += ' AND'
            query += ' author_id = %(owner_id)s'
            has_previous_case = True
        if is_closed is not None:
            if has_previous_case:
                query += ' AND'
            query += ' is_closed = %(is_closed)s'
        with g.db.cursor() as cursor:
            cursor.execute(
                query,
                {
                    'repo_id': repo_id,
                    'assignee_id': assignee_id,
                    'owner_id': owner_id,
                    'is_closed': is_closed
                }
            )
            db_rows = cursor.fetchall()
        issues = [Issue(db_row) for db_row in db_rows]
        for issue in issues:
            issue.author = User.get_by_id(issue.author_id)
            issue.assignee = User.get_by_id(issue.assignee_id) \
                if issue.assignee_id is not None else None
            issue.labels = Label.get_from_issue(issue.id)
            if issue.milestone_id is not None:
                issue.milestone = Milestone.get_by_id(issue.milestone_id)
        return issues

    @staticmethod
    def get_by_id(issue_id: int) -> Optional['Issue']:
        select_query = 'SELECT * FROM issue WHERE id = %s;'
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (issue_id,))
            db_row = cursor.fetchone()
        if db_row is None:
            return None
        issue = Issue(db_row)
        issue.author = User.get_by_id(issue.author_id)
        issue.assignee = User.get_by_id(issue.assignee_id) \
            if issue.assignee_id is not None else None
        issue.labels = Label.get_from_issue(issue.id)
        if issue.milestone_id is not None:
            issue.milestone = Milestone.get_by_id(issue.milestone_id)
        return issue

    @staticmethod
    def get_by_milestone(milestone_id: int) -> List['Issue']:
        select_query = 'SELECT * FROM issue WHERE milestone_id = %s;'
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (milestone_id,))
            db_rows = cursor.fetchall()
        issues = [Issue(db_row) for db_row in db_rows]
        for issue in issues:
            issue.author = User.get_by_id(issue.author_id)
            issue.assignee = User.get_by_id(issue.assignee_id) \
                if issue.assignee_id is not None else None
            issue.labels = Label.get_from_issue(issue.id)
            if issue.milestone_id is not None:
                issue.milestone = Milestone.get_by_id(issue.milestone_id)
        return issues

    @staticmethod
    def get_from_repo(repo_id: int) -> List['Issue']:
        select_query = '''
        SELECT id FROM issue WHERE repo_id = %s
        ORDER BY created_at DESC;
        '''
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (repo_id,))
            db_rows = cursor.fetchall()
            ids = [db_row[0] for db_row in db_rows]
        issues = [Issue.get_by_id(issue_id) for issue_id in ids]
        for issue in issues:
            issue.author = User.get_by_id(issue.author_id)
            issue.assignee = User.get_by_id(issue.assignee_id) \
                if issue.assignee_id is not None else None
            issue.labels = Label.get_from_issue(issue.id)
            if issue.milestone_id is not None:
                issue.milestone = Milestone.get_by_id(issue.milestone_id)
        return issues

    @staticmethod
    def get_number_of_closed_from_milestone(milestone_id: int) -> int:
        select_query = 'SELECT COUNT(*) FROM issue WHERE milestone_id = %s AND is_closed = true;'
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (milestone_id,))
            return cursor.fetchone()[0]
