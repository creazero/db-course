import datetime
from typing import Optional, List

from pydantic import BaseModel


class UserDTO(BaseModel):
    avatar: Optional[str]
    id: int
    username: str
    full_name: Optional[str]
    email: str
    num_repos: int
    created_at: datetime.datetime
    updated_at: datetime.datetime
    token: Optional[str]

    accessible_repos: List[int]

    class Config:
        orm_mode = True


class UsersListDTO(BaseModel):
    users: List[UserDTO]

    class Config:
        orm_mode = True
