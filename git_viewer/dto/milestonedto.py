import datetime
from typing import List, Optional

from pydantic import BaseModel


class MilestoneDTO(BaseModel):
    id: int
    repo_id: int
    title: str
    description: Optional[str]
    num_issues: int
    num_closed: Optional[int]
    due_date: Optional[datetime.date]
    created_at: datetime.datetime
    updated_at: datetime.datetime

    class Config:
        orm_mode = True


class MilestoneListDTO(BaseModel):
    milestones: List[MilestoneDTO] = []

    class Config:
        orm_mode = True
