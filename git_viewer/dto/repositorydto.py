import datetime
from typing import List, Optional

from pydantic import BaseModel

from git_viewer.dto.userdto import UserDTO


class RepositoryDTO(BaseModel):
    id: int
    owner_id: int
    owner: Optional[UserDTO]
    name: str
    description: str
    num_issues: int
    is_private: bool
    created_at: datetime.datetime
    updated_at: datetime.datetime
    contributors: List[UserDTO] = []

    class Config:
        orm_mode = True


class RepositoryListDTO(BaseModel):
    repos: List[RepositoryDTO]

    class Config:
        orm_mode = True
