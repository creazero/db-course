import datetime
from typing import Optional, List

from pydantic import BaseModel

from git_viewer.dto.labeldto import LabelDTO
from git_viewer.dto.milestonedto import MilestoneDTO
from git_viewer.dto.repositorydto import RepositoryDTO
from git_viewer.dto.userdto import UserDTO


class IssueDTO(BaseModel):
    id: int
    author: UserDTO
    author_id: int
    repo_id: int
    title: str
    content: str
    assignee_id: Optional[int]
    assignee: Optional[UserDTO]
    is_closed: bool
    milestone_id: Optional[int]
    milestone: Optional[MilestoneDTO]
    num_comments: int
    created_at: datetime.datetime
    updated_at: datetime.datetime
    labels: List[LabelDTO] = []
    repository: Optional[RepositoryDTO] = None

    class Config:
        orm_mode = True


class IssueListDTO(BaseModel):
    issues: List[IssueDTO]

    class Config:
        orm_mode = True
