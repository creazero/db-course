import datetime
from typing import Optional, List

from pydantic import BaseModel

from git_viewer.dto.userdto import UserDTO
from git_viewer.models.comment import CommentType


class CommentDTO(BaseModel):
    id: int
    type: CommentType
    author_id: int
    author: UserDTO
    issue_id: int
    repo_id: int
    old_title: Optional[str]
    new_title: Optional[str]
    commit_hash: Optional[str]
    line: Optional[int]
    content: Optional[str]
    review_id: Optional[int]
    created_at: datetime.datetime
    updated_at: datetime.datetime

    class Config:
        orm_mode = True


class CommentListDTO(BaseModel):
    comments: List[CommentDTO]

    class Config:
        orm_mode = True
