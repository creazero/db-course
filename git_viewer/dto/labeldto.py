from typing import Optional, List

from pydantic import BaseModel


class LabelDTO(BaseModel):
    id: int
    title: str
    description: Optional[str]
    color: str
    repo_id: int

    class Config:
        orm_mode = True


class LabelListDTO(BaseModel):
    labels: List[LabelDTO]

    class Config:
        orm_mode = True
