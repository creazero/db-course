from typing import Optional

from git_viewer.models.user import User


def jwt_identity(payload: int) -> Optional[User]:
    return User.get_by_id(payload)


def identity_loader(user: User) -> int:
    return user.id
