from flask import Blueprint, request, g
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_pydantic import validate

from git_viewer.dto.repositorydto import RepositoryDTO, RepositoryListDTO
from git_viewer.dto.userdto import UsersListDTO
from git_viewer.exceptions import OccupiedRepositoryName, UnknownRepository
from git_viewer.models.repository import Repository
from git_viewer.models.user import User

router = Blueprint('repositories', __name__)


@router.route('/repositories', methods=['POST'])
@jwt_required
@validate()
def create_repository():
    repository_data = request.json
    repo_name = repository_data.get('name')
    current_user_id = get_jwt_identity()
    if Repository.is_repo_name_occupied(repo_name, current_user_id):
        raise OccupiedRepositoryName
    repository = Repository(**repository_data, owner_id=current_user_id)
    repository.save()
    return RepositoryDTO.from_orm(repository)


@router.route('/repositories', methods=['GET'])
@jwt_required
@validate()
def get_repositories():
    is_current_user = request.args.get('current_user', None)
    current_user_id = get_jwt_identity()
    if is_current_user is not None:
        repos = Repository.get_by_user_id(current_user_id)
        return RepositoryListDTO(repos=repos)
    user_id = request.args.get('user_id', None)
    if user_id is not None:
        repos = Repository.get_by_user_id(user_id)
        accessible_repos = set(Repository.get_accessible_for_user(current_user_id))
        repos = [
            repo
            for repo in repos
            if not repo.is_private or repo.id in accessible_repos
        ]
        return RepositoryListDTO(repos=repos)
    repos = Repository.get_all(current_user_id)
    for repo in repos:
        repo.owner = User.get_by_id(repo.owner_id)
    return RepositoryListDTO(repos=repos)


@router.route('/repositories/validate', methods=['POST'])
@jwt_required
def validate_repository_name():
    repo_name = request.json.get('name', None)
    user_id = get_jwt_identity()
    if repo_name is not None and Repository.is_repo_name_occupied(repo_name, user_id):
        return {'valid': False}
    return {'valid': True}


@router.route('/repositories/<repository_id>', methods=['PATCH'])
@jwt_required
@validate()
def patch_repository(repository_id: int):
    repository = Repository.get_by_id(repository_id)
    if repository is None:
        raise UnknownRepository
    patch_data = request.json
    repository.merge(patch_data)
    repository.contributors = repository.get_contributors()
    if 'contributors' in patch_data:
        repository.replace_contributors(patch_data['contributors'])
    repository.save()
    return RepositoryDTO.from_orm(repository)


@router.route('/repositories/<repository_id>', methods=['DELETE'])
@jwt_required
@validate()
def delete_repository(repository_id: int):
    repository = Repository.get_by_id(repository_id)
    if repository is None:
        raise UnknownRepository
    repository.delete()
    g.db.commit()
    return {'removed': True}


@router.route('/repositories/<repository_id>/contributors', methods=['GET'])
@jwt_required
@validate()
def get_repository_contributors(repository_id: int):
    repository = Repository.get_by_id(repository_id)
    if repository is not None:
        users = repository.get_contributors()
        return UsersListDTO(users=users)


@router.route('/repositories/<username>/<repo_name>', methods=['GET'])
@jwt_required
@validate()
def get_repository(username: str, repo_name: str):
    user = User.get_by_username(username)
    if not user:
        raise UnknownRepository
    repository = Repository.get_repository_by_name_and_owner(user.id, repo_name)
    if not repository:
        raise UnknownRepository
    repository.contributors = repository.get_contributors()
    if repository.is_private:
        current_user_id = get_jwt_identity()
        contributors = set(contributor.id for contributor in repository.get_contributors())
        if current_user_id not in contributors:
            raise UnknownRepository
    return RepositoryDTO.from_orm(repository)
