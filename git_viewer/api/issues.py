from flask import Blueprint, request, g
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_pydantic import validate

from git_viewer.dto.issuedto import IssueDTO, IssueListDTO
from git_viewer.exceptions import UnknownIssue
from git_viewer.models.comment import Comment
from git_viewer.models.issue import Issue
from git_viewer.models.repository import Repository
from git_viewer.models.user import User

router = Blueprint('issues', __name__)


@router.route('/issues', methods=['POST'])
@jwt_required
@validate()
def create_issue():
    json_data = request.json
    current_user_id = get_jwt_identity()
    with g.db:
        labels = json_data.pop('labels')
        issue = Issue.create_issue(author_id=current_user_id, **json_data)
        issue.replace_labels(labels)
    return IssueDTO.from_orm(issue), 201


@router.route('/issues/<issue_id>', methods=['GET'])
@jwt_required
@validate()
def get_issue(issue_id: int):
    issue = Issue.get_by_id(issue_id)
    if issue is None:
        raise UnknownIssue
    return IssueDTO.from_orm(issue)


@router.route('/issues/<issue_id>', methods=['PATCH'])
@jwt_required
@validate()
def patch_issue(issue_id: int):
    issue = Issue.get_by_id(issue_id)
    if issue is None:
        raise UnknownIssue
    patch_data: dict = request.json
    with g.db:
        label_ids = patch_data.pop('labels', None)
        if label_ids is not None:
            issue.replace_labels(label_ids)
        is_closed = patch_data.get('is_closed', None)
        if is_closed is not None and is_closed != issue.is_closed:
            Comment.create_state_comment(
                get_jwt_identity(),
                issue.id,
                issue.repo_id,
                is_closed
            )
        issue.merge(patch_data).save()
    return IssueDTO.from_orm(issue)


@router.route('/issues/<issue_id>', methods=['DELETE'])
@jwt_required
def delete_issue(issue_id: int):
    issue = Issue.get_by_id(issue_id)
    if issue is None:
        raise UnknownIssue
    issue.delete()
    return {'success': True}


@router.route('/issues', methods=['GET'])
@jwt_required
@validate()
def get_issues():
    repository_id = request.args.get('repositoryId', None)
    milestone_id = request.args.get('milestoneId', None)
    if repository_id is not None:
        issues = Issue.get_from_repo(int(repository_id))
        return IssueListDTO(issues=issues)
    elif milestone_id is not None:
        issues = Issue.get_by_milestone(int(milestone_id))
        return IssueListDTO(issues=issues)
    raise NotImplementedError


@router.route('/issues_search', methods=['POST'])
@jwt_required
@validate()
def search_issue():
    query_result = Issue.search(**request.json)
    if len(query_result) > 0:
        accessible_repos = set(Repository.get_accessible_for_user(get_jwt_identity()))
        data = {}
        issues = []
        for issue in query_result:
            if issue.repo_id in data:
                is_private = data[issue.repo_id]
            else:
                repo = Repository.get_by_id(issue.repo_id)
                data[repo.id] = repo.is_private
                is_private = repo.is_private
            if not is_private or issue.repo_id in accessible_repos:
                issues.append(issue)
        for issue in issues:
            issue.repository = Repository.get_by_id(issue.repo_id)
            issue.repository.owner = User.get_by_id(issue.repository.owner_id)
    return IssueListDTO(issues=query_result)
