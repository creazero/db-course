from flask import Blueprint, request, g
from flask_jwt_extended import jwt_required
from flask_pydantic import validate

from git_viewer.dto.milestonedto import MilestoneDTO, MilestoneListDTO
from git_viewer.exceptions import UnknownMilestone
from git_viewer.models.issue import Issue
from git_viewer.models.milestone import Milestone

router = Blueprint('milestones', __name__)


@router.route('/milestones', methods=['POST'])
@jwt_required
@validate()
def create_milestone():
    milestone = Milestone(**request.json)
    milestone.save()
    return MilestoneDTO.from_orm(milestone)


@router.route('/milestones/<milestone_id>', methods=['PUT'])
@jwt_required
@validate()
def update_milestone(milestone_id: int):
    milestone = Milestone.get_by_id(milestone_id)
    if milestone is None:
        raise UnknownMilestone
    put_data = request.json
    milestone.title = put_data['title']
    milestone.description = put_data['description']
    milestone.due_date = put_data['due_date']
    milestone.save()
    return MilestoneDTO.from_orm(milestone)


@router.route('/milestones/<milestone_id>', methods=['DELETE'])
@jwt_required
@validate()
def delete_milestone(milestone_id: int):
    milestone = Milestone.get_by_id(milestone_id)
    if milestone is None:
        raise UnknownMilestone
    milestone.delete()
    g.db.commit()
    return {'success': True}


@router.route('/milestones', methods=['GET'])
@jwt_required
@validate()
def get_milestones():
    repository_id = request.args.get('repoId', None)
    if repository_id is not None:
        milestones = Milestone.get_from_repo(repository_id)
        for milestone in milestones:
            milestone.num_closed = Issue.get_number_of_closed_from_milestone(milestone.id)
        return MilestoneListDTO(milestones=milestones)
    raise NotImplementedError


@router.route('/milestones/<milestone_id>', methods=['GET'])
@jwt_required
@validate()
def get_milestone(milestone_id: int):
    milestone = Milestone.get_by_id(milestone_id)
    if milestone is None:
        raise UnknownMilestone
    return MilestoneDTO.from_orm(milestone)
