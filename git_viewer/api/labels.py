from flask import Blueprint, request, g
from flask_jwt_extended import jwt_required
from flask_pydantic import validate

from git_viewer.dto.labeldto import LabelDTO, LabelListDTO
from git_viewer.exceptions import UnknownLabel
from git_viewer.models.label import Label

router = Blueprint('labels', __name__)


@router.route('/labels', methods=['POST'])
@jwt_required
@validate()
def create_label():
    label_data = request.json
    label = Label(**label_data)
    label.save()
    issues_count = label.get_issues_count()
    return LabelDTO.from_orm(label)


@router.route('/labels/<label_id>', methods=['PUT'])
@jwt_required
@validate()
def update_label(label_id: int):
    label = Label.get_by_id(label_id)
    if label is None:
        raise UnknownLabel
    label.merge(request.json)
    label.save()
    return LabelDTO.from_orm(label)


@router.route('/labels/<label_id>', methods=['DELETE'])
@jwt_required
@validate()
def delete_label(label_id: int):
    label = Label.get_by_id(label_id)
    if label is None:
        raise UnknownLabel
    label.delete()
    g.db.commit()
    return {'success': True}


@router.route('/labels', methods=['GET'])
@jwt_required
@validate()
def get_labels_for_repo():
    repo_id = request.args.get('repoId', None)
    if repo_id is None:
        raise NotImplemented
    labels = Label.get_from_repo(repo_id)
    return LabelListDTO(labels=labels)
