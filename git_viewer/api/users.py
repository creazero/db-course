from datetime import timedelta

from flask import Blueprint, request
from flask_jwt_extended import create_access_token, jwt_required, get_current_user, get_jwt_identity
from flask_pydantic import validate

from git_viewer.dto.userdto import UserDTO, UsersListDTO
from git_viewer.exceptions import NotUniqueCredentials, InvalidCredentials, UnknownUser
from git_viewer.models.repository import Repository
from git_viewer.models.user import User

router = Blueprint('users', __name__)


@router.route('/login', methods=['POST'])
@validate()
def login():
    user_data = request.json
    user = User.get_by_username(user_data.get('username'))
    if user is None or not user.check_password(user_data.get('password')):
        raise InvalidCredentials
    delta = timedelta(days=30)
    user.token = create_access_token(
        identity=user,
        fresh=True,
        expires_delta=delta
    )
    return UserDTO.from_orm(user)


@router.route('/signup', methods=['POST'])
@validate()
def signup():
    user_info: dict = request.json
    username = user_info.get('username', None)
    email = user_info.get('email', None)
    if (username is not None and email is not None
            and not User.is_credentials_unique(username, email)):
        raise NotUniqueCredentials
    encrypted_password = User.hash_password(user_info.pop('password'))
    user = User(**user_info, password=encrypted_password)
    user.save(commit=True)
    return UserDTO.from_orm(user)


@router.route('/email_availability', methods=['POST'])
@jwt_required
def is_email_available():
    data = request.json
    current_user_id = get_jwt_identity()
    user = User.get_by_email(data['email'])
    if user is None or user.id == current_user_id:
        return {'available': True}
    return {'available': False}


@router.route('/users/<user_id>', methods=['PATCH'])
@jwt_required
@validate()
def update_user(user_id: int):
    user = User.get_by_id(user_id)
    if user is None:
        raise UnknownUser
    patch_data = request.json
    if 'password' in patch_data:
        patch_data['password'] = User.hash_password(patch_data['password'])
    user.merge(patch_data).save(commit=True)
    return UserDTO.from_orm(user)


@router.route('/current_user', methods=['GET'])
@jwt_required
@validate()
def current_user():
    user = get_current_user()
    user.accessible_repos = Repository.get_accessible_for_user(user.id)
    return UserDTO.from_orm(user)


@router.route('/users/<username>', methods=['GET'])
@jwt_required
@validate()
def get_user(username: str):
    user = User.get_by_username(username)
    if user is None:
        raise UnknownUser
    return UserDTO.from_orm(user)


@router.route('/users', methods=['GET'])
@jwt_required
@validate()
def get_users():
    users = User.get_all()
    for user in users:
        user.accessible_repos = Repository.get_accessible_for_user(user.id)
    return UsersListDTO(users=users)


@router.route('/validate_password', methods=['POST'])
@jwt_required
def validate_password():
    password = request.json.pop('password')
    current_user_id = get_jwt_identity()
    user = User.get_by_id(current_user_id)
    is_password_valid = user.check_password(password)
    return {'valid': is_password_valid}
