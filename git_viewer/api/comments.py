from flask import Blueprint, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_pydantic import validate

from git_viewer.dto.commentdto import CommentDTO, CommentListDTO
from git_viewer.models.comment import Comment
from git_viewer.models.user import User

router = Blueprint('comments', __name__)


@router.route('/comments', methods=['POST'])
@jwt_required
@validate()
def create_comment():
    json_data = request.json
    current_user_id = get_jwt_identity()
    comment = Comment.create_plain_comment(
        **json_data,
        author_id=current_user_id
    )
    return CommentDTO.from_orm(comment)


@router.route('/comments', methods=['GET'])
@jwt_required
@validate()
def get_comments():
    issue_id = request.args.get('issueId', None)
    if issue_id is not None:
        comments = Comment.get_by_issue_id(issue_id)
        for comment in comments:
            comment.author = User.get_by_id(comment.author_id)
        return CommentListDTO(comments=comments)
    raise NotImplementedError
