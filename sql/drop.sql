drop trigger if exists on_watch_change on watch;
drop function if exists watch_change;
drop table if exists watch;

drop trigger if exists on_star_change on star;
drop function if exists star_change;
drop table if exists star;

drop table if exists repo_access;
drop table if exists pull_request;
drop table if exists public_key;
drop table if exists issue_tag;
drop table if exists label;
drop table if exists follow;
drop table if exists copied_repo;

drop trigger if exists on_comment_change on comment;
drop function if exists comment_change;
drop table if exists comment;

drop table if exists review;

drop trigger if exists on_issue_milestone_change on issue;
drop function if exists issue_milestone_change;
drop trigger if exists on_issue_change on issue;
drop function if exists issue_change;
drop table if exists issue;

drop table if exists milestone;

drop trigger if exists on_repository_change on repository;
drop function if exists repository_change;
drop trigger if exists add_access_on_repository_insertion on repository;
drop function if exists access_on_repository_insertion;
drop table if exists repository;

drop table if exists t_user;
