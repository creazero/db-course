CREATE TABLE t_user
(
    id         SERIAL PRIMARY KEY,
    username   VARCHAR(50)              NOT NULL UNIQUE CHECK (char_length(username) > 3),
    full_name  VARCHAR(100),
    email      VARCHAR(100)             NOT NULL UNIQUE CHECK (email LIKE '%@%'),
    password   VARCHAR(150)             NOT NULL,
    num_repos  INTEGER                  NOT NULL DEFAULT 0,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone not null DEFAULT CURRENT_TIMESTAMP
);

create table repository
(
    id          serial primary key,
    owner_id    integer                  not null references t_user,
    name        varchar(50)              not null,
    description varchar(150),
    num_issues  integer                  not null default 0,
    is_private  bool                     not null default false,
    created_at  timestamp with time zone not null default current_timestamp,
    updated_at  timestamp with time zone not null default current_timestamp,
    UNIQUE (owner_id, name)
);

create function repository_change()
    returns trigger
as
$$
BEGIN
    IF TG_OP = 'INSERT' THEN
        update t_user
        set num_repos = num_repos + 1
        where id = NEW.owner_id;
        return NEW;
    ELSIF TG_OP = 'UPDATE' THEN
        update t_user
        set num_repos = num_repos - 1
        where id = OLD.owner_id;
        update t_user
        set num_repos = num_repos + 1
        where id = NEW.owner_id;
        return NEW;
    ELSIF TG_OP = 'DELETE' THEN
        update t_user
        set num_repos = num_repos - 1
        where id = OLD.owner_id;
        return OLD;
    END IF;
END;
$$ language plpgsql;

create trigger on_repository_change
    after insert or update of owner_id or delete
    on repository
    for each row
execute procedure repository_change();

create function access_on_repository_insertion()
    returns trigger
as
$$
begin
    insert into repo_access (user_id, repo_id) VALUES (new.owner_id, new.id);
    return new;
end;
$$ language plpgsql;

create trigger add_access_on_repository_insertion
    after insert
    on repository
    for each row
execute procedure access_on_repository_insertion();

create table milestone
(
    id          serial primary key,
    repo_id     integer                  not null references repository on delete cascade,
    title       varchar(100)             not null check (char_length(title) > 0),
    description varchar(1000),
    num_issues  integer                  not null default 0,
    due_date    date,
    created_at  timestamp with time zone not null default current_timestamp,
    updated_at  timestamp with time zone not null default current_timestamp
);

create table issue
(
    id           serial primary key,
    author_id    integer                  not null references t_user,
    repo_id      integer                  not null references repository on delete cascade,
    title        varchar(100)             not null check (char_length(title) > 0),
    content      varchar(10000),
    assignee_id  integer references t_user,
    is_closed    bool                     not null default false,
    milestone_id integer                  references milestone on delete set null,
    num_comments integer                  not null default 0,
    created_at   timestamp with time zone not null default current_timestamp,
    updated_at   timestamp with time zone not null default current_timestamp
);

CREATE INDEX issue_repo_id ON issue (repo_id);

create function issue_change()
    returns trigger
as
$$
BEGIN
    IF TG_OP = 'INSERT' THEN
        update repository
        set num_issues = num_issues + 1
        where id = NEW.repo_id;
        return NEW;
    ELSIF TG_OP = 'UPDATE' THEN
        update repository
        set num_issues = num_issues - 1
        where id = OLD.repo_id;
        update repository
        set num_issues = num_issues + 1
        where id = NEW.repo_id;
        return NEW;
    ELSIF TG_OP = 'DELETE' THEN
        update repository
        set num_issues = num_issues - 1
        where id = OLD.repo_id;
        return OLD;
    END IF;
END;
$$ language plpgsql;

create trigger on_issue_change
    after insert or update of repo_id or delete
    on issue
    for each row
execute procedure issue_change();

create function issue_milestone_change()
    returns trigger
as
$$
BEGIN
    IF TG_OP = 'INSERT' THEN
        update milestone
        set num_issues = num_issues + 1
        where id = NEW.milestone_id;
        return NEW;
    ELSIF TG_OP = 'UPDATE' THEN
        update milestone
        set num_issues = num_issues - 1
        where id = OLD.milestone_id;
        update milestone
        set num_issues = num_issues + 1
        where id = NEW.milestone_id;
        return NEW;
    ELSIF TG_OP = 'DELETE' THEN
        update milestone
        set num_issues = num_issues - 1
        where id = OLD.milestone_id;
        return OLD;
    END IF;
END;
$$ language plpgsql;

create trigger on_issue_milestone_change
    after insert or update of milestone_id or delete
    on issue
    for each row
execute procedure issue_milestone_change();

create table comment
(
    id         serial primary key,
    type       integer                  not null check ( type between 0 and 15 ),
    author_id  integer                  not null references t_user,
    issue_id   integer                  not null references issue on delete cascade,
    repo_id    integer                  not null references repository on delete cascade,
    content    varchar(10000),
    created_at timestamp with time zone not null default current_timestamp,
    updated_at timestamp with time zone not null default current_timestamp
);

CREATE INDEX comment_issue_id ON comment (issue_id);

create function comment_change()
    returns trigger
as
$$
BEGIN
    IF TG_OP = 'INSERT' THEN
        IF new.type = 0 THEN
            update issue
            set num_comments = num_comments + 1
            where id = NEW.issue_id;
        end if;
        return NEW;
    ELSIF TG_OP = 'UPDATE' THEN
        IF new.type = 0 THEN
            update issue
            set num_comments = num_comments - 1
            where id = OLD.issue_id;
            update issue
            set num_comments = num_comments + 1
            where id = NEW.issue_id;
        END IF;
        return NEW;
    ELSIF TG_OP = 'DELETE' THEN
        if new.type = 0 then
            update issue
            set num_comments = num_comments - 1
            where id = OLD.issue_id;
        end if;
        return OLD;
    END IF;
END;
$$ language plpgsql;

create trigger on_comment_change
    after insert or update of repo_id or delete
    on comment
    for each row
execute procedure comment_change();

create table label
(
    id          serial primary key,
    title       varchar(20) not null,
    description varchar(1000),
    color       varchar(7)  not null default '#999999' check ( char_length(color) = 7 ),
    repo_id     integer     not null references repository on delete cascade
);

CREATE INDEX label_repo_id ON label (repo_id);

create table issue_tag
(
    id       serial primary key,
    label_id integer not null references label on delete cascade,
    issue_id integer not null references issue on delete cascade,
    UNIQUE (label_id, issue_id)
);

create table repo_access
(
    id      serial primary key,
    user_id integer not null references t_user on delete cascade,
    repo_id integer not null references repository on delete cascade,
    UNIQUE (user_id, repo_id)
);
